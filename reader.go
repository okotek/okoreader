package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/okotek/okoframe"
	"gitlab.com/okotek/okolog"
	"gitlab.com/okotek/okonet"
	"gitlab.com/okotek/okoutils"
	"gocv.io/x/gocv"
)

var pDat okolog.LogPreDat

var deadReadCount int = 1

func main() {

	pDat = okolog.SetLogPreDat("localhost:8067", "localhost:8066", "meee")
	outchan := make(chan okoframe.Frame)

	go okonet.SendoffHandler(outchan, "localhost:8082")

	go readVideoFile(outchan)

	for {
		time.Sleep(100 * time.Hour)
	}
}

func readVideoFile(output chan okoframe.Frame) {
	var tmpTags []okoframe.TagList = []okoframe.TagList{}

	mat := gocv.NewMat()
	compMat := gocv.NewMat()

	vidList, er := okoutils.FindActiveCams(0, 3)
	if er != nil {
		fmt.Println(er)
		os.Exit(1)
	}

	fmt.Println("vidList:", vidList)

	vid, err := gocv.VideoCaptureDevice(0)
	if err != nil {
		fmt.Println("WOOPS:", err)
	}

	roundCount := 0

	for {
		tst := vid.Read(&mat)
		if !tst {
			deadReadCount++
			fmt.Println("Dead read count:", deadReadCount)
			continue
		}

		if compMat.Empty() || roundCount%20 == 0 {
			compMat = mat
			fmt.Println("___________________________________________________________________________-")
		}

		frm := okoframe.CreateFrameFromExisting(mat, "0", "", tmpTags, 0, time.Now(), "", "", okoutils.RandStr(30))

		frm.MotionPercentChange = compareFrames(mat, compMat)

		output <- frm

		roundCount++

		time.Sleep(100 * time.Millisecond)

	}

}

func dumpPixels(input gocv.Mat, comp gocv.Mat) {
	if input.Cols() != comp.Cols() || input.Rows() != comp.Rows() {
		fmt.Println("Images do not match.")
		return
	}

	byteArr := input.ToBytes()

	for _, i := range byteArr {
		fmt.Println(i)
	}

	fmt.Println("Ending dump.")
}

func compareFrames(newFrame gocv.Mat, compFrame gocv.Mat) int {
	arr1 := newFrame.ToBytes()
	arr2 := compFrame.ToBytes()

	for iter := range arr1 {
		if arr1[iter] != arr2[iter] {
			fmt.Println(arr1[iter], "-", arr2[iter], "=", arr1[iter]-arr2[iter])
		} else {
			//fmt.Println(arr1[iter], "-", arr2[iter], "=", arr1[iter]-arr2[iter])
		}
	}

	return 0
}
