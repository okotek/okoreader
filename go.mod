module gitlab.com/okotek/okoreader

go 1.13

require (
	gitlab.com/okotek/okoframe v0.0.0-20220105061727-f995ae7b7d70
	gitlab.com/okotek/okolog v0.0.0-20220217032221-85f0884e366e // indirect
	gitlab.com/okotek/okonet v0.0.0-20220105062037-f3db659d2547
	gitlab.com/okotek/okoutils v0.0.0-00010101000000-000000000000
	gocv.io/x/gocv v0.29.0
)

replace gitlab.com/okotech/okoframe => ../okoframe/

replace gitlab.com/okotech/okonet => ../okonet/

replace gitlab.com/okotek/okonet => ../okonet/

replace gitlab.com/okotek/okoutils => ../okoutils

replace gitlab.com/okotek/okoframe => ../okoframe
